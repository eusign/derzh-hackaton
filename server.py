from flask import Flask, render_template, abort, Response
from bs4 import BeautifulSoup
app = Flask(__name__)

@app.route("/EDRAPI/ws")
def wsdl():
    return Response(render_template('broken.wsdl.xml'), mimetype='text/xml;charset=utf-8')

@app.route("/500")
def p500():
    xml = request.stream.read()
    pxml = BeautifulSoup(xml)
    print(pxml.find('login'), pxml.find('password'))
    abort(500)


if __name__ == "__main__":
    app.run(port=7003, host="0.0.0.0", debug=True)
